import { readFile, readFileSync } from 'fs';
import path from 'path';
import * as vscode from 'vscode';

export class DriftstackPanel implements vscode.WebviewViewProvider {
    private _view: vscode.WebviewView | undefined;
    private portData: any;
    getWebviewContent(ports: Array<any>) {
        // You can customize the HTML content here
        const config = this.getConfig();
        const workspaceDomain = "space.app.driftstack.com";
        return `<!DOCTYPE html>
            <html>
            <head>
                <title>Driftstack</title>
                <style>
                    body {
                        font-family: Arial, sans-serif;
                        padding: 0;
                        margin: 0;
                    }
                    table {
                        border-collapse: collapse;
                        width: 100%;
                    }
                    table thead td {
                        padding: 13px;
                    }
                    table td {
                        padding: 5px;
                        padding-left: 13px;
                    }
                    table thead td {
                        font-weight: bold;
                        font-size: 16px;
                        border-bottom: 1px solid #242424;
                    }
                    table tbody td {
                        border-bottom: 1px solid #242424;
                    }
                </style>
            </head>
            <body>
                <table>
                    <thead>
                        <tr>
                            <td>Port</td>
                            <td>Name</td>
                            <td>Url</td>
                        </tr>
                    </thead>
                    <tbody>
                    ${(function fun() {
                        let portHtml = "";
                        ports.forEach((port: any) => {
                            const url = `https://${port.name}-${config.get('uid')}.${workspaceDomain}`;
                            portHtml += `
                                <tr>
                                    <td class="port">${port.port}</td>
                                    <td class="name">${port.name}</td>
                                    <td class="url"><a href="${url}">${url}</a> </td>
                                </tr>
                            `;
                        });
                        return portHtml;
                    })()}
                    </tbody>
                </table>
            </body>
            </html>
        `;
    }

    resolveWebviewView(webviewView: vscode.WebviewView, context: vscode.WebviewViewResolveContext<unknown>, token: vscode.CancellationToken): void | Thenable<void> {
        this._view = webviewView;
        webviewView.webview.options={enableScripts:true};
        this.requestPortData().then((json:any) => {
            webviewView.webview.html=this.getWebviewContent(json.data);
        });
        webviewView.webview.onDidReceiveMessage(data => {
            console.log(data);
		});
    }

    parseConfig(data: any): Map<any, any> {
        const config: Map<any, any> = new Map();
        const lines = data.split('\n');
        for (const line of lines) {
            const [key, value] = line.split('=');
            config.set(key, value);
        }
        return config;
    }
    getConfig(): Map<any, any> {
        const homeDir = process.env.HOME;
        if (homeDir) {
            const file = readFileSync(path.join(homeDir, '.ds','config'));
            return this.parseConfig(""+file);
        }
        return new Map();
    }
    async requestPortData() {
        const config = this.getConfig();
        const url = config.get('api_url');
        const token = config.get('token');
        const uid = config.get('uid');

        
        return await fetch(url+"instance/"+uid+"/ports", 
        {
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer '+token
            }
        }
        ).then(response => response.json());
    }
}